﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Net.Sockets;
using System.Net;
using System.Net.NetworkInformation;
using System;

public class SlmpMenager : MonoBehaviour
{
    [SerializeField] TMP_Text textDebugToShow;
    [SerializeField] float timeInterval = 0.1f;
    [SerializeField] TMP_Text textPosX, textPosY, textPosZ, textPosC;
    [SerializeField] TMP_Text textPosXMin, textPosXMax, textPosYMin, textPosYMax, textPosZMin, textPosZMax, textAngleCMin, textAngleCMax;
    [SerializeField] TMP_Text textJogSpeed;

    bool performRead;
    float currentTiemToCommunication = 0;

    AllReadedData allReadedData;
    private void Start()
    {
        scriptHandler = GameObject.Find("AllScriptsHandler");
        labelsName = new string[]
        {
            "MachinePosX", "MachinePosY", "MachinePosZ", "MachineAngleC",
            "MachineLimitXmin", "MachineLimitXmax", "MachineLimitYmin", "MachineLimitYmax",
            "MachineLimitZmin", "MachineLimitZmax", "MachineLimitCmin", "MachineLimitCmax",
            "JogSpeedCurrent"
        };
        allReadedData = new AllReadedData();
    }
    public struct AllReadedData
    {
        public bool correctReaded;
        public Vector3 currentPosition;
        public double currentAngleC;
        public Vector3 machineLimitMax;
        public Vector3 machineLimitMin;
        public double machineLimitAngleCMin;
        public double machineLimitAngleCMax;
        public int jogSpeedCurrent;
    }
    public AllReadedData GetAllReadedData()
    {
        return allReadedData;
    }
    public void CommunicationButtonPress()
    {
        if (!performRead)
            StartSlmpFuntions();
        else
            StopSlmpFunctions();
    }
    public void StartSlmpFuntions()
    {
        if(!tcpC.Connected)
        {
            textDebugToShow.text = "Perform start!";
            TestConnection();
        }
    }
    public void StopSlmpFunctions()
    {
        textDebugToShow.text = "Perform stop!";
        performRead = false;
        tcpC.Close();
    }
    int countOfperformSlmp = 0;
    private void Update()
    {
        if(performRead)
        {
            currentTiemToCommunication -= Time.deltaTime;
            if(currentTiemToCommunication <= 0.0f)
            {
                countOfperformSlmp++;
                textDebugToShow.text = $"Perform read {countOfperformSlmp}";
                ReadRandomSlmp();
                WriteRandomSlmp();
                currentTiemToCommunication = timeInterval;
            }
        }
    }
    private TcpClient tcpC = new TcpClient();
    private void TestConnection()
    {
        tcpC = new TcpClient();
        byte[] byteAdress = new byte[4];
        byteAdress[0] = 192; byteAdress[1] = 168; byteAdress[2] = 3; byteAdress[3] = 39;
        IPAddress ipAdress = new IPAddress(byteAdress);
        if (true)//MakePingTest(ipAdress))
        {
            ConnectTCP(ipAdress, 2000);
            if (tcpC.Connected)
            {
                if (SelfTest())
                {
                    textDebugToShow.text += "\nAll tests PASS";
                    performRead = true;
                    currentTiemToCommunication = timeInterval;
                }
                else
                {
                    textDebugToShow.text += "\nBinary set FAIL";
                }
            }
            else
            {
                textDebugToShow.text += "\nNo available SLMP connection";
            }
        }
    }
    private bool MakePingTest(IPAddress IPAddressForTest)
    {
        bool pingAns = false;
        System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping();
        PingReply reply = pingSender.Send(IPAddressForTest);
        if (reply.Status == IPStatus.Success)
        {
            pingAns = true;
        }
        return pingAns;
    }
    private void ConnectTCP(IPAddress IPAddressToConnect, int portNumber)
    {
        tcpC.ReceiveTimeout = 5;
        tcpC.SendTimeout = 5;
        try
        {
            tcpC = Connect(IPAddressToConnect, portNumber, 1000);
        }
        catch
        {
            textDebugToShow.text += "\nPort Open FAIL";
        }
    }
    private TcpClient Connect(IPAddress hostName, int port, int timeout)
    {
        var client = new TcpClient();
        var state = new State { Client = client, Success = true };
        System.IAsyncResult ar = client.BeginConnect(hostName, port, EndConnect, state);
        state.Success = ar.AsyncWaitHandle.WaitOne(timeout, false);
        if (!state.Success || !client.Connected)
            throw new System.Exception("Failed to connect.");
        return client;
    }
    private class State
    {
        public TcpClient Client { get; set; }
        public bool Success { get; set; }
    }
    private void EndConnect(System.IAsyncResult ar)
    {
        var state = (State)ar.AsyncState;
        TcpClient client = state.Client;
        try
        {
            client.EndConnect(ar);
        }
        catch { }
        if (client.Connected && state.Success)
            return;
        client.Close();
    }
    private bool SelfTest()
    {
        bool loopTestAns = false;
        byte[] loopMessage = new byte[5] { 0x41, 0x42, 0x43, 0x44, 0x45 };
        int needByteMessage = 2 + 4 + 2 + loopMessage.Length;
        byte lowByte = (byte)(needByteMessage & 0xff);
        byte highByte = (byte)(needByteMessage >> 8 & 0xff);

        byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, lowByte, highByte, 0x10, 0x00,
                                          0x19, 0x06,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        lowByte = (byte)(loopMessage.Length & 0xff);
        highByte = (byte)(loopMessage.Length >> 8 & 0xff);
        payload[15] = lowByte; payload[16] = highByte;
        for (int i = 0; i < loopMessage.Length; i++)
        {
            payload[17 + i] = loopMessage[i];
        }
        NetworkStream stream = tcpC.GetStream();
        stream.Write(payload, 0, payload.Length);
        byte[] data = new byte[20];
        stream.ReadTimeout = 1000;
        try
        {
            int bytes = stream.Read(data, 0, data.Length);
            if (data[9] == 0 && data[10] == 0 && data[11] == lowByte && data[12] == highByte)
            {
                loopTestAns = true;
                for (int i = 0; i < loopMessage.Length; i++)
                {
                    if (loopMessage[i] != data[13 + i])
                    {
                        loopTestAns = false;
                    }
                }
            }
        }
        catch
        {
            loopTestAns = false;
        }
        return loopTestAns;
    }
    public enum DataTypeId { Bit = 1, WordUnsigned = 2, DoubleWordUnsigned = 3, WordSigned = 4, DoubleWordSigned = 5, SinglePrecisionReal = 6, DoublePrecisionReal = 7, Hour = 8, String = 9, Unicode = 10 };
    public struct RandomReadResponse
    {
        public DataTypeId dataTypeId;
        public int readDataLength;
        public byte[] rawData;
        string dataAfterConversion;
        public string DecodeData()
        {
            switch (dataTypeId)
            {
                case DataTypeId.Bit:
                    dataAfterConversion = System.Convert.ToBoolean(rawData[0]).ToString();
                    break;
                case DataTypeId.WordUnsigned:
                    dataAfterConversion = System.BitConverter.ToUInt16(rawData, 0).ToString();
                    break;
                case DataTypeId.DoubleWordUnsigned:
                    dataAfterConversion = System.BitConverter.ToUInt32(rawData, 0).ToString();
                    break;
                case DataTypeId.WordSigned:
                    dataAfterConversion = System.BitConverter.ToInt16(rawData, 0).ToString();
                    break;
                case DataTypeId.DoubleWordSigned:
                    dataAfterConversion = System.BitConverter.ToInt32(rawData, 0).ToString();
                    break;
                case DataTypeId.SinglePrecisionReal:
                    dataAfterConversion = System.BitConverter.ToSingle(rawData, 0).ToString();
                    break;
                case DataTypeId.DoublePrecisionReal:
                    dataAfterConversion = System.BitConverter.ToDouble(rawData, 0).ToString();
                    break;
                case DataTypeId.Hour:
                    dataAfterConversion = "Row time: ";
                    for (int i = 0; i < rawData.Length; i++)
                    {
                        dataAfterConversion += $", [{i}] {rawData[i]}";
                    }
                    break;
                case DataTypeId.String:
                    dataAfterConversion = System.Text.Encoding.UTF8.GetString(rawData);
                    break;
                case DataTypeId.Unicode:
                    dataAfterConversion = System.Text.Encoding.Unicode.GetString(rawData);
                    break;
            }
            return dataAfterConversion;
        }
        public string GetDecodeData()
        {
            return dataAfterConversion;
        }
    }
    string[] labelsName;
    private void ReadRandomSlmp()
    {
        allReadedData.correctReaded = false;
        int totalLengthOfLabel = 0;
        for (int i = 0; i < labelsName.Length; i++)
        {
            totalLengthOfLabel += labelsName[i].Length;
        }
        int needByteMessage = 2 + 4 + 4 + (2 * (labelsName.Length + totalLengthOfLabel));
        byte lowByte = (byte)(needByteMessage & 0xff);
        byte highByte = (byte)(needByteMessage >> 8 & 0xff);
        byte lowNumberOfReadDataPoints = (byte)(labelsName.Length & 0xff);
        byte highNumberOfReadDataPoints = (byte)(labelsName.Length >> 8 & 0xff);
        byte[] beginingPart = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, lowByte, highByte, 0x10, 0x00,
                                          0x1C, 0x04, 0x00, 0x00, lowNumberOfReadDataPoints, highNumberOfReadDataPoints, 0x00, 0x00};
        int totalSizeOfFrame = 9 + needByteMessage;
        byte[] payload = new byte[totalSizeOfFrame];
        for (int i = 0; i < beginingPart.Length; i++)
        {
            payload[i] = beginingPart[i];
        }
        byte lowLabelNameLength = 0, highLabelNameLength = 0;
        byte[] afterDecode;
        int indexOfBeginig = beginingPart.Length;
        for (int i = 0; i < labelsName.Length; i++)
        {
            lowLabelNameLength = (byte)(labelsName[i].Length & 0xff);
            highLabelNameLength = (byte)(labelsName[i].Length >> 8 & 0xff);
            payload[indexOfBeginig + 0] = lowLabelNameLength;
            payload[indexOfBeginig + 1] = highLabelNameLength;
            afterDecode = System.Text.Encoding.Unicode.GetBytes(labelsName[i]);
            for (int j = 0; j < labelsName[i].Length; j++)
            {
                payload[indexOfBeginig + 2 + (2 * j)] = afterDecode[(2 * j) + 0];
                payload[indexOfBeginig + 3 + (2 * j)] = afterDecode[(2 * j) + 1];
            }
            indexOfBeginig += 2 + (2 * labelsName[i].Length);
        }

        if (tcpC.Connected)
        {
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new byte[256];
            stream.ReadTimeout = 1000;
            try
            {
                int bytes = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    textDebugToShow.text += "\nOk random label read";
                    int numberOfReadDataPoints;
                    numberOfReadDataPoints = (int)data[11] + 0x100 * ((int)data[12]);
                    RandomReadResponse tempResponse;
                    string feedbackDecode;
                    int indexForCurrentData = 13;
                    for (int i = 0; i < numberOfReadDataPoints; i++)
                    {
                        tempResponse = new RandomReadResponse();
                        tempResponse.dataTypeId = (DataTypeId)data[indexForCurrentData];
                        tempResponse.readDataLength = (int)data[indexForCurrentData + 2] + 0x100 * ((int)data[indexForCurrentData + 3]);
                        tempResponse.rawData = new byte[tempResponse.readDataLength];
                        for (int j = 0; j < tempResponse.readDataLength; j++)
                        {
                            tempResponse.rawData[j] = data[indexForCurrentData + 4 + j];
                        }
                        feedbackDecode = tempResponse.DecodeData();
                        switch (i)
                        {
                            case 0:
                                allReadedData.currentPosition.x = float.Parse(feedbackDecode) / 10000.0f;
                                textPosX.text = allReadedData.currentPosition.x.ToString("F04");
                                break;
                            case 1:
                                allReadedData.currentPosition.y = float.Parse(feedbackDecode) / 10000.0f;
                                textPosY.text = allReadedData.currentPosition.y.ToString("F04");
                                break;
                            case 2:
                                allReadedData.currentPosition.z = float.Parse(feedbackDecode) / 10000.0f;
                                textPosZ.text = allReadedData.currentPosition.z.ToString("F04");
                                break;
                            case 3:
                                allReadedData.currentAngleC = double.Parse(feedbackDecode) / 100000;
                                textPosC.text = allReadedData.currentAngleC.ToString("F05");
                                break;
                            case 4:
                                allReadedData.machineLimitMin.x = float.Parse(feedbackDecode) / 10000.0f;
                                textPosXMin.text = allReadedData.machineLimitMin.x.ToString("F04");
                                break;
                            case 5:
                                allReadedData.machineLimitMax.x = float.Parse(feedbackDecode) / 10000.0f;
                                textPosXMax.text = allReadedData.machineLimitMax.x.ToString("F04");
                                break;
                            case 6:
                                allReadedData.machineLimitMin.y = float.Parse(feedbackDecode) / 10000.0f;
                                textPosYMin.text = allReadedData.machineLimitMin.y.ToString("F04");
                                break;
                            case 7:
                                allReadedData.machineLimitMax.y = float.Parse(feedbackDecode) / 10000.0f;
                                textPosYMax.text = allReadedData.machineLimitMax.y.ToString("F04");
                                break;
                            case 8:
                                allReadedData.machineLimitMin.z = float.Parse(feedbackDecode) / 10000.0f;
                                textPosZMin.text = allReadedData.machineLimitMin.z.ToString("F04");
                                break;
                            case 9:
                                allReadedData.machineLimitMax.z = float.Parse(feedbackDecode) / 10000.0f;
                                textPosZMax.text = allReadedData.machineLimitMax.z.ToString("F04");
                                break;
                            case 10:
                                allReadedData.machineLimitAngleCMin = double.Parse(feedbackDecode) / 100000;
                                textAngleCMin.text = allReadedData.machineLimitAngleCMin.ToString("F05");
                                break;
                            case 11:
                                allReadedData.machineLimitAngleCMax = double.Parse(feedbackDecode) / 100000;
                                textAngleCMax.text = allReadedData.machineLimitAngleCMax.ToString("F05");
                                break;
                            case 12:
                                allReadedData.jogSpeedCurrent = int.Parse(feedbackDecode);
                                textJogSpeed.text = allReadedData.jogSpeedCurrent.ToString();
                                break;
                        }
                        textDebugToShow.text += "\nType of data: " + tempResponse.dataTypeId.ToString() + ", readed data: " + feedbackDecode;
                        indexForCurrentData += 4 + tempResponse.readDataLength;
                    }
                    allReadedData.correctReaded = true;
                }
                else
                {
                    int errorEndCode = (int)data[9] + 0x100 * ((int)data[10]);
                    textDebugToShow.text += "\nError read, End code: " + errorEndCode.ToString("x");
                }
            }
            catch
            {
                textDebugToShow.text += "\nError in interpreter";
            }
        }
        else
        {
            textDebugToShow.text += "\nStream diconnected, no possible read";
            StopSlmpFunctions();
            StartSlmpFuntions();
            if(!tcpC.Connected)
            {
                StopSlmpFunctions();
            }
        }       
    }
    public struct RandomWriteData
    {
        public string labelName;
        public DataTypeId dataTypeId;
        public byte[] dataRaw;
    }
    GameObject scriptHandler;
    private void WriteRandomSlmp()
    {
        List<RandomWriteData> listLabels = new List<RandomWriteData>();
        RandomWriteData tempRandomWriteData;

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "ComBeat";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if(scriptHandler.GetComponent<UiTests>().comBeatValue)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "IncresseJogSpeed";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().jogSpeedInc)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "DecresseJogSpeed";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().jogSpeedDec)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "EnableJogMotionMode";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().enableJogMotionMode)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "DisableJogMotionMode";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().disableJogMotionMode)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "PerformFbMoveToPoint";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().performFbMoveToPoint)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "PerformFbPickAndPlace";
        tempRandomWriteData.dataTypeId = DataTypeId.Bit;
        tempRandomWriteData.dataRaw = new byte[2];
        if (scriptHandler.GetComponent<GamePadValue>().performFbPickAndPlace)
        {
            tempRandomWriteData.dataRaw[0] = 1;
        }
        else
        {
            tempRandomWriteData.dataRaw[0] = 0;
        }
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "LeftAxisStickX";
        tempRandomWriteData.dataTypeId = DataTypeId.DoublePrecisionReal;
        tempRandomWriteData.dataRaw = new byte[8];
        double leftX = scriptHandler.GetComponent<GamePadValue>().leftStickVector2d.x;
        tempRandomWriteData.dataRaw = BitConverter.GetBytes(leftX);
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "LeftAxisStickY";
        tempRandomWriteData.dataTypeId = DataTypeId.DoublePrecisionReal;
        tempRandomWriteData.dataRaw = new byte[8];
        double leftY = scriptHandler.GetComponent<GamePadValue>().leftStickVector2d.y;
        tempRandomWriteData.dataRaw = BitConverter.GetBytes(leftY);
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "RightAxisStickX";
        tempRandomWriteData.dataTypeId = DataTypeId.DoublePrecisionReal;
        tempRandomWriteData.dataRaw = new byte[8];
        double rightX = scriptHandler.GetComponent<GamePadValue>().rightStickVector2d.x;
        tempRandomWriteData.dataRaw = BitConverter.GetBytes(rightX);
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "RightAxisStickY";
        tempRandomWriteData.dataTypeId = DataTypeId.DoublePrecisionReal;
        tempRandomWriteData.dataRaw = new byte[8];
        double rightY = scriptHandler.GetComponent<GamePadValue>().rightStickVector2d.y;
        tempRandomWriteData.dataRaw = BitConverter.GetBytes(rightY);
        listLabels.Add(tempRandomWriteData);

        tempRandomWriteData = new RandomWriteData();
        tempRandomWriteData.labelName = "GripperState";
        tempRandomWriteData.dataTypeId = DataTypeId.WordSigned;
        tempRandomWriteData.dataRaw = new byte[2];
        short gripperState = scriptHandler.GetComponent<GamePadValue>().gripperState;
        tempRandomWriteData.dataRaw = BitConverter.GetBytes(gripperState);
        listLabels.Add(tempRandomWriteData);

        int totalLengthOfLabel = 0;
        int totallengthOfDataSize = 0;
        for (int i = 0; i < listLabels.Count; i++)
        {
            totalLengthOfLabel += listLabels[i].labelName.Length;
            totallengthOfDataSize += listLabels[i].dataRaw.Length;
        }
        int needByteMessage = 2 + 4 + 4 + ((2 + 2) * listLabels.Count) + (2 * totalLengthOfLabel) + totallengthOfDataSize;
        byte lowNeedByteMessage = (byte)(needByteMessage & 0xff);
        byte highNeedByteMessage = (byte)(needByteMessage >> 8 & 0xff);
        byte lowNumberOfWriteDataPint = (byte)(listLabels.Count & 0xff);
        byte highNumberOfWriteDataPint = (byte)(listLabels.Count >> 8 & 0xff);
        byte[] beginingPart = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, lowNeedByteMessage, highNeedByteMessage, 0x10, 0x00,
                                          0x1B, 0x14, 0x00, 0x00, lowNumberOfWriteDataPint, highNumberOfWriteDataPint, 0x00, 0x00};
        int totalSizeOfFrame = 9 + needByteMessage;
        byte[] payload = new byte[totalSizeOfFrame];
        for (int i = 0; i < beginingPart.Length; i++)
        {
            payload[i] = beginingPart[i];
        }
        byte lowLabelNameLength = 0, highLabelNameLength = 0, lowWriteDataLength = 0, highWriteDataLength = 0;
        byte[] afterDecode;
        int indexOfBeginig = beginingPart.Length;
        for (int i = 0; i < listLabels.Count; i++)
        {
            lowLabelNameLength = (byte)(listLabels[i].labelName.Length & 0xff);
            highLabelNameLength = (byte)(listLabels[i].labelName.Length >> 8 & 0xff);
            payload[indexOfBeginig + 0] = lowLabelNameLength;
            payload[indexOfBeginig + 1] = highLabelNameLength;
            afterDecode = System.Text.Encoding.Unicode.GetBytes(listLabels[i].labelName);
            for (int j = 0; j < listLabels[i].labelName.Length; j++)
            {
                payload[indexOfBeginig + 2 + (2 * j)] = afterDecode[(2 * j) + 0];
                payload[indexOfBeginig + 3 + (2 * j)] = afterDecode[(2 * j) + 1];
            }
            lowWriteDataLength = (byte)(listLabels[i].dataRaw.Length & 0xff);
            highWriteDataLength = (byte)(listLabels[i].dataRaw.Length >> 8 & 0xff);
            payload[indexOfBeginig + 2 + (2 * listLabels[i].labelName.Length)] = lowWriteDataLength;
            payload[indexOfBeginig + 3 + (2 * listLabels[i].labelName.Length)] = highWriteDataLength;
            for (int j = 0; j < listLabels[i].dataRaw.Length; j++)
            {
                payload[indexOfBeginig + 4 + (2 * listLabels[i].labelName.Length) + j] = listLabels[i].dataRaw[j];
            }
            indexOfBeginig += 4 + (2 * listLabels[i].labelName.Length) + listLabels[i].dataRaw.Length;
        }

        if (tcpC.Connected)
        {
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[100];
            stream.ReadTimeout = 1000;
            try
            {
                Int32 bytes = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    textDebugToShow.text += "\nOk random label write";
                }
                else
                {
                    int errorEndCode = (int)data[9] + 0x100 * ((int)data[10]);
                    textDebugToShow.text += "\nError random label write, End code: " + errorEndCode.ToString("x");
                }
            }
            catch
            {
                textDebugToShow.text += "\nError in interpreter";
            }
        }
        else
        {
            textDebugToShow.text += "\nStream diconnected, no possible write";
        }
    }
}