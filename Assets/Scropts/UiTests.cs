using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiTests : MonoBehaviour
{
    [SerializeField] float timeSwitch = 0.2f;
    [SerializeField] Image comBeatStatus;
    [SerializeField] TextMeshProUGUI comStatusText;
    [SerializeField] TMP_InputField ipAdressValue;
    [SerializeField] TMP_InputField portNumberValue;
    void Update()
    {
        if (isPerformedConnection)
        {
            currentDeltaTimeValue -= Time.deltaTime;
            if (currentDeltaTimeValue < 0)
            {
                comBeatValue = !comBeatValue;
                currentDeltaTimeValue += timeSwitch;
                if (comBeatValue)
                    PerformComBeatColorChange(Color.blue);
                else
                    PerformComBeatColorChange(Color.cyan);
            }
        }
    }
    bool isPerformedConnection = false;
    float currentDeltaTimeValue = 0.0f;
    public bool comBeatValue = false;
    public void ConnectionButtonPressed()
    {
        isPerformedConnection = !isPerformedConnection;
        if(isPerformedConnection)
        {
            currentDeltaTimeValue = 0.0f;
            comBeatValue = false;
            comStatusText.text = "Connection status\n" + "OK " + ipAdressValue.text + ":" + portNumberValue.text;
        }
        else
        {
            PerformComBeatColorChange(Color.yellow);
            comStatusText.text = "Connection status\n" + "Disconnected";
        }
    }
    private void PerformComBeatColorChange(Color newColor)
    {
        comBeatStatus.color = newColor;
    }
}